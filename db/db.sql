-- Q1.Containerize MySQL and create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name) table inside that. 

create table Movie(movie_id int auto_increment,
movie_title varchar(100),
movie_release_date date,
movie_time time,
director_name varchar(100));