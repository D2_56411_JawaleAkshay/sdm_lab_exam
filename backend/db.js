const mysql=require('mysql')

const pool=mysql.createPool({
    host:'demodb',
    user:'root',
    password:'root',
    port:3306,
    waitForConnections:true,
    connectionLimit:10,
    queueLimit:0
})

module.exports=pool