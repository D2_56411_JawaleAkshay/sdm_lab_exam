const express=require('express')
const db=require('../db')

// Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)

const router=express.Router()

router.post('/',(request,response)=>{
    const{movie_title,movie_release_date,movie_time,director_name}=request.body

    const query=`insert into Movies (movie_title,movie_release_date,movie_time,director_name)
                     values ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`

    db.execute(query,(error,result)=>{
        response.send(error,result)
    })
})


router.get('/',(request,response)=>{
    const query=`select * from Movies`

    db.execute(query,(error,result)=>{
        response.send(error,result)
    })
})

router.delete('/:id',(request,response)=>{
    const {id}=request.params

    const query=`delete * from Movies where movie_id='${id}'`

    db.execute(query,(error,result)=>{
        response.send(error,result)
    })
})

// UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table
router.put('/:id',(request,response)=>{
    const {id}=request.params
    const {movie_release_date, movie_time }=request.body

    const query=`update movies set movie_release_date='${movie_release_date}',movie_time='${movie_time}'  where movie_id='${id}'`

    db.execute(query,(error,result)=>{
        response.send(error,result)
    })
})




module.exports=router
