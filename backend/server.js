const express=require('express')

const app=express()

app.use(express.json())

const routerMovies=require('./Routes/movies')

app.use('/movies',routerMovies)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server started on 4000');
})